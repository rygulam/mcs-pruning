/*
 * SVM.h
 *
 *  Created on: Feb 7, 2011
 *      Author: rgreen
 */

#ifndef SVM_H_
#define SVM_H_

#include "Classifier.h"
#include "OPFClassifier.h"
#include "MTRand.h"
#include "libsvm/svm.h"

class SVMClassifier : public Classifier {

    public:
        SVMClassifier();
        SVMClassifier(std::string curSystem, int n, std::vector < Generator > g, std::vector < Bus > b);
        SVMClassifier(std::string curSystem, int n, std::vector < Generator > g, std::vector < Bus > b, std::vector < Line > t);
        SVMClassifier(std::string curSystem, int n, std::vector < Generator > g, std::vector < Bus > b, std::vector < Line > t, Classifier* cl);
        virtual ~SVMClassifier();
        virtual SVMClassifier* clone() const; 

        void init();
        void load();
        void reset();
        void test();

        void addLoad(int busNumber, double amount){}
        void addLoad(std::vector<double> amounts){}

        void setVerbose(bool v);

        double run(std::string curSolution, double& excess);
        double run(std::vector<double> curSolution, double& excess);
        double getTrainingTime();
        double getClassificationTime();
        double getSensitivity();
        double getSpecificity();
        double getAccuracy();
        double getPrecision();

        void setValidateClassification(bool b);
        void setNumTrainingStates(int nts);


    private:
        double classify(std::string curSolution, double& excess);
        double classify(std::vector<int> curSolution, double& excess);
        double classify(std::vector<double> curSolution, double& excess);
        void   scaleSolution(std::vector<double>& curSolution);

        void generateTrainingData();
        void scale();
        void train();
        void initialize();

        int numTrainingStates;
        double falsePositives, falseNegatives,
               trueNegatives, truePositives,
               lineAdjustment, trainingTime;
        bool trained, verbose, validateClassification;


        std::vector<double> scaleMin, scaleMax;
        std::vector < std::vector < double> > 	trainingInput, trainingOutput;

        Classifier* classifier;

        svm_model	*svmModel;
        svm_problem *svmProblem;
        svm_parameter *svmParameters;
};

#endif /* SVM_H_ */
